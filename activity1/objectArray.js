let courses = [];

const postCourse = (id, courseName, description, price, isActive) => {
  courses.push({
    id: id,
    courseName: courseName,
    description: description,
    price: price,
    isActive: isActive,
  });
  alert(`You have created ${courseName}.The price is ${price}.`);
  // console.log(`You have created ${courseName}.The price is ${price}.`);
};

postCourse("A01", "Science", "DIFFICULTYYYY", 1250, false);
postCourse("B02", "Math", "PISSY EASSY", 1000, true);
console.log(courses);

const getSingleCourse = (id) => {
  let foundCourse = courses.find((course) => id === course.id);
  if (foundCourse) {
    console.log("Course founded, detail is:");
    console.log(foundCourse);
  } else {
    console.log("Cour--------se not found.");
  }
};

getSingleCourse("B02");
getSingleCourse("NO");

const deleteCourse = () => {
  courses.pop();
};

deleteCourse();

console.log(courses);
