let users = [
  {
    username: "mikeg90o",
    email: "michaeljames@gmail.com",
    password: "mikecutie1999",
    isAdmin: false,
  },
  {
    username: "justingg",
    email: "justintlake@gmail.com",
    password: "iamnsynceo",
    isAdmin: true,
  },
];

console.log(users[0]);
console.log(users[1]);
console.log(users[1].email);
console.log(users[0].username);

users.push({
  username: "abbeyarcher",
  email: "abbeyarrows@gmail.com",
  password: "bowandabbey",
  isAdmin: false,
});

console.log(users);

let newUser = {
  username: "hanna1993",
  email: "hannafight@gmail.com",
  password: "fighting1993",
  isAdmin: false,
};

users.push(newUser);
console.log(users);

class User {
  constructor(username, email, password) {
    this.username = username;
    this.email = email;
    this.password = password;
    this.isAdmin = false;
  }
}

let user1 = new User("aaa", "ccc@vvv", "bbb");
console.log(user1);

let user2 = new User("picotaro", "Picotaro@gmail.com", "this is a pen");
let user3 = new User("monmon", "ketoppi@hotmail.com", "I have an apple");

users.push(user2);
users.push(user3);

console.log(users);

users.push(new User("newuser1", "newuseremail@gmail.com", "newuser&"));
console.log(users);

let starNum = 10;
for (let i = 0; i <= starNum; i++) {
  let star = "";
  for (let j = 0; j <= starNum; j++) {
    if (i > j) {
      star += "*";
    }
  }
  console.log(star);
}

for (let i = 0; i <= starNum; i++) {
  let star = "";
  for (let j = starNum; j >= 0; j--) {
    if (i == j) {
      star += "*";
    } else {
      star += " ";
    }
  }
  console.log(star);
}

function register(username, email, password) {
  if (username.length < 8 || email.length < 8 || password.length < 8) {
    console.log(
      "Details provided too short. username,email and password must be more than 8 characters."
    );
    return;
  }
  users.push({
    username: username,
    email: email,
    password: password,
    isAdmin: false,
  });
}

register("aaa", "masahiro@gmail.com", "gogogogogo");
register("masahiro@gmail.com", "aaa", "gogogogogo");
register("masahiro@gmail.com", "masahiro@gmail.com", "gogogogogo");
console.log(users);

function login(emailInput, pwInput) {
  console.log(emailInput);
  console.log(pwInput);
  let foundUser = users.find((user) => {
    // console.log(user);
    return user.email === emailInput && user.password === pwInput;
  });
  if(foundUser !== undefined){
      console.log("Thank you for log in");
  }else {
      console.log("Invalid.No user found");
  }
  console.log(foundUser)
}
login("masahiro@gmail.com", "gogogogogo");

